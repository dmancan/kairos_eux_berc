

	<?php if( is_home() || is_category() || is_tag() || is_search()  || is_singular('aof')) : ?>
		<div class="infinite-post">
			<div class="row">
				<div class="col-sm-8">
					<p class="date small"><?php the_time('M. jS, Y'); ?></p>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p><?php the_excerpt(); ?></p>

					<span class="alert alert-info"><?php the_category( ', ' ); ?></span>
				</div>
				<div class="col-sm-4">
					<?php if( has_post_thumbnail() ): ?>
						<a href="<?php the_permalink(); ?>" class="hidden-xs"><?php the_post_thumbnail('blog-thumb',array( 'class'	=> "img-responsive"));?></a>

					<?php endif; ?>
				</div>
			</div>
			<hr>
		</div>
	<?php endif; ?>
	<?php if( is_front_page() ): ?>
		<?php if( has_post_thumbnail() ): ?>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('homepage-thumb',array( 'class'	=> "img-responsive"));?></a>
		<?php endif; ?>
			<p class="date small"><?php the_time('M. jS, Y'); ?></p>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php the_excerpt(); ?>
	<?php endif; ?>

