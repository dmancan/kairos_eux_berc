<?php /* Template Name: Lessons */
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<?php get_template_part( 'parts/hero'); ?>
<section class="container">
	<div class="row">
		<div class="col-md-8">
			<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
			<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro', false, false).'</div>';?>
			<?php the_content(); ?>
			<?php $lessons_posts = new WP_Query(array(
					'post_type' => 'lessons',
					'showposts' => -1,
					'orderby' => 'menu_order',
				));?>
			<?php if( $lessons_posts->have_posts()):?>
			<div class="posts">
				<?php while( $lessons_posts->have_posts()): $lessons_posts->the_post();?>
				<div class="row">
					<div class="col-md-5">
						<?php if (has_post_thumbnail()){the_post_thumbnail('lessons-post-thumbnail');}?>
					</div>
					<div class="col-md-7">
						<article>
							<h4><?php the_title()?></h4>
							<?php the_excerpt();?>
							<hr/>
							<p><a href="<?php the_permalink()?>" class="btn btn-primary">Learn more</a></p>
						</article>
					</div>
				</div>
			<?php endwhile;?>
			</div>
			<?php endif; wp_reset_postdata();?>
		</div>
		<?php get_template_part( 'parts/sidebar'); ?>
	</div>
</section>

<?php endwhile; endif; ?>
<?php get_footer(); ?>