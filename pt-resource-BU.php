<?php /* Template Name: Resource Centre */
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<section class="hero promo">
	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<h1><span><?php _e( "The Blanket Exercise", "kairos" ); ?></span>
				<?php the_title(); ?></h1>
				<h2><?php if(get_field('_page_intro')) echo get_field('_page_intro', false, false); ?></h2>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<?php dynamic_sidebar('interior-page-sidebar'); ?>
			</div>
		</div>
	</section>
	<div class="bg"></div>
</section>
<?php if(have_rows('call_out')): while(have_rows('call_out')): the_row()?>
<section class="container">
	<div class="row">
		<div class="col-md-8">
			<article>
				<h3><?php the_sub_field('left_block');?></h3>
			</article>
		</div>
		<div class="col-md-3 col-md-offset-1">
			<?php the_sub_field('right_block');?>
		</div>
		<hr/>
	</div>
	<hr/>
</section>
<?php endwhile; endif;?>
<?php $child_pages = get_pages(array(
	'child_of' => get_the_ID(),
	'sort_column' => 'menu_order',
	));?>
<?php if(  $child_pages ):?>
<section class="container posts">
	<?php foreach( $child_pages as $post):?>
	<?php setup_postdata( $post );?>
	<div class="row">
		<div class="col-md-4">
			<?php if (has_post_thumbnail()){the_post_thumbnail('resource-post-thumbnail');}?>
		</div>
		<div class="col-md-8">
			<article>
				<h4><?php the_title()?></h4>
				<?php the_excerpt();?>
				<hr/>
				<p><a href="<?php the_permalink()?>" class="btn btn-primary">Learn more</a></p>
			</article>
		</div>
	</div>
<?php endforeach; wp_reset_postdata();?>
</section>		
<?php endif; ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>