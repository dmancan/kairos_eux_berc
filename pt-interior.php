<?php /* Template Name: Interior page */
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<section class="hero">
	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><span><?php _e( "The Blanket Exercise", "kairos" ); ?></span>
				<?php the_title(); ?></h1>
				<h2 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php if(get_field('_page_intro')) echo get_field('_page_intro', false, false); ?></h2>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<?php dynamic_sidebar('interior-page-sidebar'); ?>
			</div>
		</div>
	</section>
	<div class="bg"></div>
</section>
<?php if(have_rows('call_out')): while(have_rows('call_out')): the_row()?>

<section class="container downloads">
	<div class="row">
		<div class="col-md-8">
			<article>
				<h3><?php the_sub_field('left_block');?></h3>
			</article>
		</div>
		<div class="col-md-3 col-md-offset-1">
			<h2>Download Package</h2>
			<?php if(get_sub_field('right_block')):?>
			<?php 
				$ch = curl_init(get_sub_field('right_block'));
				 curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				 curl_setopt($ch, CURLOPT_HEADER, TRUE);
				 curl_setopt($ch, CURLOPT_NOBODY, TRUE);
				 $data = curl_exec($ch);
				 $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
				 curl_close($ch);
				 $size = round($size/1048576, 2);
				?>
			
			<h4><?php echo $size ?> MB (PDF)</h4>
			<br/>
			<a href="<?php the_sub_field('right_block');?>" class="btn btn-primary">Download</a>
			
			<?php endif;?>
		</div>
		
	</div>
	<hr/>
</section>

<?php endwhile; endif;?>
<section class="container">
		<div class="row">
			<div class="col-md-8">
				<article>
					<?php the_content(); ?>
				</article>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<?php if( have_rows('videos') || is_active_sidebar('interior-page-products-sidebar')) : ?>
					<?php while( have_rows('videos') ) : the_row(); ?>
						<?php the_sub_field('video'); ?>
						<hr>
					<?php endwhile; ?>
					<?php dynamic_sidebar('interior-page-products-sidebar'); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php get_template_part( 'parts/related-posts'); ?>
</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>