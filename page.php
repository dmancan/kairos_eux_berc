<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php get_template_part( 'parts/hero'); ?>
<section class="container">
	<div class="row">
		<div class="col-md-8">
			<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
			<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro', false, false).'</div>';?>
			<?php the_content(); ?>
		</div>
		<?php get_template_part( 'parts/sidebar'); ?>
	</div>
</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>