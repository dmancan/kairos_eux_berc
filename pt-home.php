<?php /* Template Name: Home  */ get_header(); ?>
	<div>
	<?php if( have_rows('heroes') ) : ?>
		<div>
			<?php while( have_rows('heroes') ) : the_row(); ?>
				<section class="hero">
					<section class="container">
						<div class="row">
							<div class="col-md-6 pull-<?php the_sub_field('alignment'); ?> col-sm-6 col-xs-12">
								<?php berc_field( '<h1>', '</h1>', 'title', true ); ?>
								<?php berc_field( '<h2>', '</h2>', 'content', true ); ?>
								<?php if( get_sub_field('call_to_action_text') && get_sub_field('call_to_action_link') ) : ?><p><a href="<?php the_sub_field('call_to_action_link'); ?>" class="btn btn-default btn-lg"><?php the_sub_field('call_to_action_text'); ?></a></p><?php endif; ?>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<?php berc_field( '<br><br><div class="vimeo">', '</div>', 'media', true ); ?>
							</div>
						<?php the_sub_field('sub_field_name'); ?>
						</div>
					</section>
					<div class="bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_sub_field('background'), 'full')[0]; ?>);"></div>
				</section>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php
	// Get the latest posts
	$args = array(
		//Type & Status Parameters
		'post_type'   => 'post',
		'posts_per_page' => 4
	);

	$query = new WP_Query( $args );

	switch_to_blog( 1 );
	// Get the latests events
	
	// $event_args = array(
		//Type & Status Parameters
		// 'post_type'   => 'tribe_events',
		// 'posts_per_page' => 2
	// );

	// $event_query = new WP_Query( $event_args );
	restore_current_blog();
	?>
	<main style="background:#b02c29">
	<!--
		<section class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<h4><?php _e( "Latest Posts", "kairos" ); ?></h4>
						</div>
						<div class="col-md-6 text-right">
							<h4><a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"><?php _e( "Show All", "kairos" ); ?></a></h4>
						</div>
					</div>
					<hr>
					<div class="row">
						<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
						<div class="col-md-3 col-sm-6">
							<?php get_template_part( 'parts/content-post' ); ?>
						</div>
						<?php endwhile; endif; wp_reset_postdata(); ?>
					</div>
				</div>

			</div>
		</section>-->
	</main>
<?php get_footer(); ?>
